var gulp = require('gulp');
var $ = require('gulp-load-plugins')();

gulp.task('sass:watch', function() {
  return gulp.watch(['./assets/scss/*.scss', './assets/scss/**/*.scss'], ['sass']);
});

gulp.task('sass', function() {
  return gulp.src(['./assets/scss/*.scss', './assets/scss/**/*.scss'])
    .pipe($.sass({ outputStyle: 'expanded' }).on('error', $.sass.logError))
    .pipe(gulp.dest('./assets/css'));
});